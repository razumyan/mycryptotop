<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Top 10 cryptocurrency by capitalization</title>


        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="top10-cap">
                    <div class="title m-b-md">
                        Top 10 cryptocurrency by capitalization
                    </div>
                    <input type="checkbox"> Compare currency with previous day
                    <table class="table" id="currencies" role="grid">
                        <thead>
                            <tr class="dark-blue">
                                <th>

                                </th>
                                <th class="col-rank text-center sorting_asc" rowspan="1" colspan="1">
                                    #
                                </th>
                                <th id="th-name" class="sortable sorting" tabindex="0" aria-controls="currencies" rowspan="1" colspan="1">
                                    Name
                                </th>
                                <th id="th-marketcap" class="sortable text-right sorting" data-mobile-text="M. Cap" tabindex="0" rowspan="1" colspan="1">
                                    Market Cap
                                </th>
                                <th id="th-price" class="sortable text-right sorting" tabindex="0" aria-controls="currencies" rowspan="1" colspan="1">
                                    Price
                                </th>
                                <th id="th-volume" class="sortable text-right sorting" data-mobile-text="Volume" tabindex="0" rowspan="1" colspan="1">
                                    Volume (24h)
                                </th>
                                <th id="th-change" class="sortable text-right sorting" data-mobile-text="Change" tabindex="0" rowspan="1" colspan="1">
                                    Change (24h)
                                </th>
                                <th id="th-marketcap-graph" class="sorting_disabled" rowspan="1" colspan="1">
                                    Price Graph (7d)
                                </th>
                                <th id="th-qty" class="sorting_disabled" rowspan="1" colspan="1">
                                    My Qty
                                </th>
                                <th id="th-total-btc blue" class="sorting_disabled" rowspan="1" colspan="1">
                                    Total, BTC
                                </th>
                                <th id="th-total-btc" class="sorting_disabled" rowspan="1" colspan="1">
                                    Total, $
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr id="id-bitcoin" class="odd" role="row">
                            <td class="text-center">
                                <i class="glyphicon glyphicon-minus-sign"></i>
                            </td>
                            <td class="text-center">
                                1
                            </td>
                            <td class="no-wrap currency-name">
                                <div class="s-s-bitcoin currency-logo-sprite"></div>
                                <span class="currency-symbol">
                                    BTC
                                </span>
                            </td>
                            <td class="no-wrap market-cap light-blue text-right">
                                $182 281 200 148
                            </td>
                            <td class="no-wrap text-right">
                                $10 905.10
                            </td>
                            <td class="no-wrap light-blue text-right">
                                $5 735 820 000
                            </td>
                            <td class="no-wrap percent-24h text-right positive_change">
                                1.74%
                            </td>
                            <td>
                                <img class="sparkline" alt="sparkline" src="https://files.coinmarketcap.com/generated/sparklines/1.png">
                            </td>
                            <td>
                                <input type="text">
                            </td>
                        </tr>
                        <tr id="add-td" class="odd" role="row">
                            <td>

                            </td>
                            <td class="text-center" colspan="3">
                                <input type="text"> <i class="glyphicon glyphicon-plus-sign"></i>
                            </td>
                            <td class="text-center" colspan="6">

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="top10-cap-volume-growth">
                    <div class="title m-b-md">
                        Top 10 cryptocurrency by volume growth from Top 100 by capitalization
                    </div>

                    <table class="table" id="currencies-volume-growth" role="grid">
                    <thead>
                    <tr class="dark-blue">
                        <th class="col-rank text-center sorting_asc" rowspan="1" colspan="1">
                            #
                        </th>
                        <th id="th-name-volume-growth" class="sortable sorting" tabindex="0" aria-controls="currencies" rowspan="1" colspan="1">
                            Name
                        </th>
                        <th id="th-marketcap-volume-growth" class="sortable text-right sorting" data-mobile-text="M. Cap" tabindex="0" rowspan="1" colspan="1">
                            Market Cap
                        </th>
                        <th id="th-price-volume-growth" class="sortable text-right sorting" tabindex="0" aria-controls="currencies" rowspan="1" colspan="1">
                            Price
                        </th>
                        <th id="th-volume-volume-growth" class="sortable text-right sorting" data-mobile-text="Volume" tabindex="0" rowspan="1" colspan="1">
                            Volume (24h)
                        </th>
                        <th id="th-change-volume-growth" class="sortable text-right sorting" data-mobile-text="Change" tabindex="0" rowspan="1" colspan="1">
                            Change (24h)
                        </th>
                        <th id="th-marketcap-graph-volume-growth" class="sorting_disabled" rowspan="1" colspan="1">
                            Price Graph (7d)
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr id="id-bitcoin-volume-growth" class="odd" role="row">
                        <td class="text-center">
                            1
                        </td>
                        <td class="no-wrap currency-name">
                            <div class="s-s-bitcoin currency-logo-sprite"></div>
                            <span class="currency-symbol">
                                BTC
                            </span>
                        </td>
                        <td class="no-wrap market-cap light-blue text-right">
                            $182 281 200 148
                        </td>
                        <td class="no-wrap text-right">
                            $10 905.10
                        </td>
                        <td class="no-wrap light-blue text-right">
                            $5 735 820 000
                        </td>
                        <td class="no-wrap percent-24h text-right positive_change">
                            1.74%
                        </td>
                        <td>
                            <img class="sparkline" alt="sparkline" src="https://files.coinmarketcap.com/generated/sparklines/1.png">
                        </td>
                    </tr>
                    </tbody>
                </table>
                </div>
                <div class="top10-price-gain">
                    <div class="title m-b-md">
                        Top 10 cryptocurrency by price gain with marketcap > $100mln
                    </div>
                    <table class="table" id="currencies-price-gain" role="grid">
                        <thead>
                        <tr class="dark-blue">
                            <th class="col-rank text-center sorting_asc" rowspan="1" colspan="1">
                                #
                            </th>
                            <th id="th-name-price-gain" class="sortable sorting" tabindex="0" aria-controls="currencies" rowspan="1" colspan="1">
                                Name
                            </th>
                            <th id="th-marketcap-price-gain" class="sortable text-right sorting" data-mobile-text="M. Cap" tabindex="0" rowspan="1" colspan="1">
                                Market Cap
                            </th>
                            <th id="th-price-price-gain" class="sortable text-right sorting" tabindex="0" aria-controls="currencies" rowspan="1" colspan="1">
                                Price
                            </th>
                            <th id="th-volume-price-gain" class="sortable text-right sorting" data-mobile-text="Volume" tabindex="0" rowspan="1" colspan="1">
                                Volume (24h)
                            </th>
                            <th id="th-change-price-gain" class="sortable text-right sorting" data-mobile-text="Change" tabindex="0" rowspan="1" colspan="1">
                                Change (24h)
                            </th>
                            <th id="th-marketcap-graph-price-gain" class="sorting_disabled" rowspan="1" colspan="1">
                                Price Graph (7d)
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr id="id-bitcoin-volume-growth" class="odd" role="row">
                            <td class="text-center">
                                1
                            </td>
                            <td class="no-wrap currency-name">
                                <div class="s-s-bitcoin currency-logo-sprite"></div>
                                <span class="currency-symbol">
                                    BTC
                                </span>
                            </td>
                            <td class="no-wrap market-cap light-blue text-right">
                                $182 281 200 148
                            </td>
                            <td class="no-wrap text-right">
                                $10 905.10
                            </td>
                            <td class="no-wrap light-blue text-right">
                                $5 735 820 000
                            </td>
                            <td class="no-wrap percent-24h text-right positive_change">
                                1.74%
                            </td>
                            <td>
                                <img class="sparkline" alt="sparkline" src="https://files.coinmarketcap.com/generated/sparklines/1.png">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <footer>
            <div class="col-xs-12 col-md-5">
                &copy; 2017 MyCryptoTop
            </div>
            <div class="col-md-1 twitter">
                <a href="https://twitter.com/MyCryptoTop">Twitter</a>
            </div>
            <div class="col-xs-12 col-md-6 text-right">
                BTC: <a href="">1KvpiBASv1agDt2Hh97mNWzP3u4CmY2WTG</a>
                <br>
                ETH: <a href="">0x5b4047c2644AE7289576cc3F29A21da8eacdA42F</a>
            </div>
        </footer>
    </body>
</html>

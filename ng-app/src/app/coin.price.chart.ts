import {Component, Input} from "@angular/core";

@Component({
  selector: 'coin-price-chart',
  template: `
    <img [src]="'data:image/bmp;base64,' + base64ImageData" class="sparkline">
  `
})
export class CoinPriceChart {

  base64ImageData: string;

  @Input('data')
  chartData: number[];

  ngOnInit() {
    let depth = 32;
    let height = 50;
    let width = 200;
    let hpadding = 5;

    let offset, rawSize, data, image, adaptedData, x, y, bgColor, smoothColor, fgColor;

    function conv(size: number) {
      return String.fromCharCode(size & 0xff, (size >> 8) & 0xff, (size >> 16) & 0xff, (size >> 24) & 0xff);
    }

    offset = 54;

    rawSize = depth * width * height;

    //BMP Header
    data = 'BM';                          // ID field
    data += conv(offset + rawSize);       // BMP size
    data += conv(0);                      // unused
    data += conv(offset);                 // pixel data offset

    //DIB Header
    data += conv(40);                      // DIB header length
    data += conv(width);                   // image width
    data += conv(height);                  // image height
    data += String.fromCharCode(1, 0);     // colour panes
    data += String.fromCharCode(depth, 0); // bits per pixel
    data += conv(0);                       // compression method
    data += conv(rawSize);                 // size of the raw data
    data += conv(2835);                    // horizontal print resolution
    data += conv(2835);                    // vertical print resolution
    data += conv(0);                       // colour palette, 0 == 2^n
    data += conv(0);                       // important colours

    bgColor = conv(0xffffff);
    fgColor = conv(0xffba00);
    smoothColor = conv(0xfdeec6);

    //Pixel data
    adaptedData = this.scaleChartData(width, height - hpadding);
    for (y = 0; y < height; y++) {
      for (x = 0; x < width; x++) {
        if (y < adaptedData[x] + 1) {
          data += smoothColor;
        } else if (y == adaptedData[x] + 1) {
          data += fgColor;
        } else if (y == adaptedData[x] + 2) {
          data += smoothColor;
        } else {
          data += bgColor;
        }
      }
    }
    this.base64ImageData = btoa(data);
  }

  private scaleChartData(width: number, height: number): number[] {
    let x, i, j, min, max, hScale, wScale, arr = [];
    min = Math.min.apply(null, this.chartData);
    max = Math.max.apply(null, this.chartData);
    if (max != min) {
      hScale = height / (max - min);
    } else {
      hScale = 1;
    }
    if (this.chartData.length > 1) {
      wScale = width / (this.chartData.length - 1);
    } else {
      wScale = width;
    }
    for (i = 0; i < this.chartData.length - 1; i++) {
      let y0 = Math.ceil((this.chartData[i] - min) * hScale);
      let y1 = Math.ceil((this.chartData[i + 1] - min) * hScale)
      for (j = 0; j < wScale; j++) {
        arr.push(Math.ceil(y0 + j * (y1 - y0) / wScale));
      }
    }
    return arr;
  }
}

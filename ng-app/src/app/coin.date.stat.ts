import {CoinMetadata} from "./coin.metadata";
import {PriceStat} from "./price.stat";

export class CoinDateStat {
  coin: CoinMetadata;
  userAmount?: number;
  dayStat: PriceStat;
  changeStat: PriceStat;
  chartData: number[];
  timestamp: number; //last_updated
}

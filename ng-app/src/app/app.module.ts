import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppMain} from './app.main';
import {FormsModule} from '@angular/forms';
import {MyCryptoTopService} from "./my.crypto.top.service";
import {UserLocalStoreService} from "./user.local.store.service";
import {CoinPriceChart} from "./coin.price.chart";


@NgModule({
  declarations: [
    AppMain,
    CoinPriceChart
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    MyCryptoTopService,
    UserLocalStoreService
  ],
  bootstrap: [AppMain]
})
export class AppModule {
}

import {Component, OnInit} from '@angular/core';
import {MyCryptoTopService} from "./my.crypto.top.service";
import {CoinGeneralStat} from "./coin.general.stat";
import {CoinDateStat} from "./coin.date.stat";
import {CoinMetadata} from "./coin.metadata";
import $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.main.html',
  styleUrls: ['./app.main.css'],
})
export class AppMain implements OnInit {
  favorites: CoinDateStat[] = [];
  foundCoins: CoinMetadata[] = [];
  topTenByVol: CoinDateStat[] = [];
  topTenByPrice: CoinDateStat[] = [];
  searchText: string = "";
  noMatched: boolean = false;
  editCoinId: string;
  editAmount: number = 0;

  constructor(private myCryptoTopService: MyCryptoTopService) {

  }

  ngOnInit() {
    this.loadData();
  }

  public editAmountOf(coinId: string, amount: number) {
    this.editCoinId = coinId;
    this.editAmount = amount;
    setTimeout(() =>  $("#favorite-amount-" + coinId).focus().select(), 100);
  }

  public applyEditedAmount() {
    if (this.editCoinId && this.editAmount >= 0.0) {
      let coinId = this.editCoinId;
      let amount = this.editAmount;
      this.editCoinId = null;
      this.editAmount = 0;
      this.myCryptoTopService.setCoinAmount(coinId, amount)
        .subscribe((data) => this.favorites = data);
    }
  }

  public cancelEditedAmount() {
    this.editCoinId = null;
    this.editAmount = 0;
  }

  public findFavorites() {
    if (this.searchText) {
      this.myCryptoTopService.findCoins(this.searchText)
        .subscribe(data => {
          this.foundCoins = data;
          this.noMatched = data.length == 0;
        });
    } else {
      this.foundCoins = [];
    }
  }

  public addToFavorites(coinId: string) {
    this.myCryptoTopService.addToFavorites(coinId)
      .subscribe(data => this.favorites = data);
  }

  public removeFromFavorites(coinId: string) {
    this.myCryptoTopService.removeFromFavorites(coinId)
      .subscribe(data => this.favorites = data);
  }

  public isFavorite(coinId: string): boolean {
    return this.favorites.find(item => item.coin.id == coinId) != null;
  }

  private loadData() {
    let that = this;
    this.myCryptoTopService.getGeneralStat().subscribe(data => that.applyGeneralData(data));
  }

  private applyGeneralData(data: CoinGeneralStat) {
    this.favorites = data.favorites;
    this.topTenByVol = data.topTenByVol;
    this.topTenByPrice = data.topTenByPrice;
  }
}

import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {CoinDateStat} from "./coin.date.stat";
import {ALL_COINS, FAVORITES, TOP_TEN_BY_PRICE, TOP_TEN_BY_VOL} from "./mock-coins";
import {CoinGeneralStat} from "./coin.general.stat";
import {UserLocalStoreService} from "./user.local.store.service";
import {CoinMetadata} from "./coin.metadata";

@Injectable()
export class MyCryptoTopService {

  private favorites: Map<string, number> = null;

  constructor(private userLocalStoreService: UserLocalStoreService) {
  }

  private fetchFavorites() {
    if (this.favorites == null) {
      this.favorites = this.userLocalStoreService.loadFavorites();
    }
    return this.favorites;
  }

  private fetchFavoritesStat() {
    this.fetchFavorites();
    return ALL_COINS.filter((item) => {
      return this.favorites.has(item.coin.id);
    }).map(item => {
      item.userAmount = this.favorites.get(item.coin.id);
      return item;
    });
  }

  private findCoinById(data: CoinDateStat[], coinId: string) {
    return data.find(function (item) {
      return item.coin.id == coinId;
    });
  }

  findCoins(searchText: string): Observable<CoinMetadata[]> {
    let matchedCoins = ALL_COINS.map(coinStat => coinStat.coin).filter(function (item) {
      return item.name.toLowerCase().indexOf(searchText.toLowerCase()) >= 0;
    });
    return of(matchedCoins);
  }

  getFavorites(): Observable<CoinDateStat[]> {
    return of(this.fetchFavoritesStat());
  }

  removeFromFavorites(coinId: string): Observable<CoinDateStat[]> {
    if (this.favorites.delete(coinId)) {
      this.userLocalStoreService.storeFavorites(this.favorites);
    }
    return of(this.fetchFavoritesStat());
  }

  addToFavorites(coinId: string): Observable<CoinDateStat[]> {
    if (!this.favorites.has(coinId)) {
      this.favorites.set(coinId, 0);
      this.userLocalStoreService.storeFavorites(this.favorites);
    }
    return of(this.fetchFavoritesStat());
  }

  getGeneralStat(): Observable<CoinGeneralStat> {
    return of({
      favorites: this.fetchFavoritesStat(),
      topTenByVol: TOP_TEN_BY_VOL,
      topTenByPrice: TOP_TEN_BY_PRICE
    });
  }

  setCoinAmount(coinId: string, value: number): Observable<CoinDateStat[]> {
    this.favorites.set(coinId, value);
    this.userLocalStoreService.storeFavorites(this.favorites);
    return of(this.fetchFavoritesStat());
  }
}

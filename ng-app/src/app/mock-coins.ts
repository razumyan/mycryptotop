import {CoinDateStat} from "./coin.date.stat";

export const ALL_COINS: CoinDateStat[] = [
  {
    coin: {
      id: "bitcoin",
      name: "Bitcoin",
      symbol: "BTC"
    },
    dayStat: {
      rank: 1,
      priceUsd: 11499.7,
      priceBtc: 1.0,
      marketCapUsd: 192258596928,
      volume: 16718575.0
    },
    changeStat: {
      rank: 0,
      priceUsd: 99.7,
      priceBtc: 0,
      marketCapUsd: 928,
      volume: 5.0
    },
    chartData: [11499.7, 12499.7, 14499.7, 17499.7, 18499.7, 14499.7, 10499.7],
    timestamp: 1512360252
  },
  {
    coin: {
      id: "ethereum",
      name: "Ethereum",
      symbol: "ETH"
    },
    dayStat: {
      rank: 2,
      priceUsd: 467.106,
      priceBtc: 0.0409823,
      marketCapUsd: 44898390324.0,
      volume: 96120346.0
    },
    changeStat: {
      rank: 0,
      priceUsd: 0.2,
      priceBtc: 0.0000023,
      marketCapUsd: 324.0,
      volume: 46.0
    },
    chartData: [427.106, 437.106, 390.106, 354.106, 312.106, 287.106, 317.106, 365.106, 467.106],
    timestamp: 1512360262
  },
  {
    coin: {
      id: "bitcoin-cash",
      name: "Bitcoin Cash",
      symbol: "BCH"
    },
    dayStat: {
      rank: 3,
      priceUsd: 1576.05,
      priceBtc: 0.138277,
      marketCapUsd: 26535421933.0,
      volume: 16836663.0
    },
    changeStat: {
      rank: 1,
      priceUsd: -76.05,
      priceBtc: -0.00077,
      marketCapUsd: 1933.0,
      volume: 63.0
    },
    chartData: [1176.05, 1376.05, 1356.05, 1576.05, 1686.05, 1434.05, 1600.05, 1476.05, 1576.05],
    timestamp: 1512360270
  },
  {
    coin: {
      id: "ripple",
      name: "Ripple",
      symbol: "XRP"
    },
    dayStat: {
      rank: 4,
      priceUsd: 0.254353,
      priceBtc: 0.00002232,
      marketCapUsd: 9823842958.0,
      volume: 38622870411.0
    },
    changeStat: {
      rank: -1,
      priceUsd: 0.000053,
      priceBtc: 0.00000002,
      marketCapUsd: -2958.0,
      volume: 411.0
    },
    chartData: [0.354353, 0.364353, 0.374353, 0.324353, 0.294353, 0.284353, 0.274353, 0.294353, 0.254353],
    timestamp: 1512360241
  }];

export const FAVORITES: CoinDateStat[] = [];

export const TOP_TEN_BY_VOL: CoinDateStat[] = ALL_COINS;

export const TOP_TEN_BY_PRICE: CoinDateStat[] = ALL_COINS;

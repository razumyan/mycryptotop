export class CoinMetadata {
  id: string;
  name: string;
  symbol: string;
}

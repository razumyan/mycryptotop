import { TestBed, async } from '@angular/core/testing';
import { AppMain } from './app.main';
describe('AppMain', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppMain
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppMain);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});

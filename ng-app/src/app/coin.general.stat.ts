import {CoinDateStat} from "./coin.date.stat";

export class CoinGeneralStat {
  favorites: CoinDateStat[];
  topTenByVol: CoinDateStat[];
  topTenByPrice: CoinDateStat[];
}

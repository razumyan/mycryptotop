export class PriceStat {
  rank: number; //rank
  priceUsd: number; //price_usd
  priceBtc: number; //price_btc
  marketCapUsd: number; //market_cap_usd
  volume: number; //24_volume_usd
}

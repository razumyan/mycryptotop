import {Injectable} from "@angular/core";

@Injectable()
export class UserLocalStoreService {

  constructor() {
  }

  public storeFavorites(coinAmounts: Map<string, number>) {
    let dataObject = {};
    for(let [entryKey, entryVal] of Array.from(coinAmounts.entries())) {
      dataObject[entryKey] = entryVal;
    }
    localStorage.setItem('favoriteAmounts', JSON.stringify(dataObject));
  }

  public loadFavorites(): Map<string, number> {
    let result: Map<string, number> = new Map();
    let favoriteAmounts = localStorage.getItem('favoriteAmounts');
    if (favoriteAmounts) {
      let parsedFavorites = JSON.parse(favoriteAmounts);
      for (let favorite in parsedFavorites) {
        result.set(favorite, parsedFavorites[favorite]);
      }
      return result;
    }
    let favorites = localStorage.getItem('favorites');
    if (favorites) {
      let parsedFavorites = JSON.parse(favorites);
      for (let favorite of parsedFavorites) {
        result.set(favorite, 0);
      }
      return result;
    }
    return result;
  }
}

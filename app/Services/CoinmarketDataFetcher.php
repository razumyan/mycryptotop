<?php

namespace App\Services;

use App\Models\Currencies;
use App\Models\CurrenciesChangeRates;
use App\Models\CurrenciesHistory;
use App\Models\CurrenciesValues;
use CoinMarketCap\Base;

class CoinmarketDataFetcher
{
    const HISTORY_GAP_IN_SECONDS = 60 * 60;

    const CURRENCIES_COUNT_LIMIT = 10; //without 0, default - 100 numbers of currencies

    private function mapToCoins($currencies)
    {
        $arr = [];
        foreach ($currencies as $val) {
            $arr[] = new Coins($val);
        }
        return $arr;
    }

    private function calcMeanPropertyValue($itemPropertyName, $currenciesHistory)
    {
        $countValues = count($currenciesHistory);
        if ($countValues > 0) {
            $sum = 0.0;
            foreach ($currenciesHistory as $item) {
                $sum += (float)$item->$itemPropertyName;
            }
            return $sum / $countValues;
        }
        return 0;
    }

    public function fetchDataAndStore()
    {
        $coinmarketcap = new Base();
        $currencies = $coinmarketcap->getTicker(['limit' => self::CURRENCIES_COUNT_LIMIT]);

        $coins = $this->mapToCoins($currencies);

        foreach ($coins as $item) {
            Currencies::updateOrCreate(
                ['id_name' => $item->id_name],
                ['name' => $item->name, 'symbol' => $item->symbol]
            );

            $curr = Currencies::select('id')->where('id_name', $item->id_name)->first();

            $currencyId = $curr['id'];

            CurrenciesValues::updateOrCreate(
                ['currencies_id' => $currencyId], //search conditions
                [
                    'rank' => $item->rank,
                    'price_usd' => (float)$item->price_usd,
                    'price_btc' => (float)$item->price_btc,
                    '24h_volume_usd' => (float)$item->volume,
                    'available_supply' => (float)$item->available_supply,
                    'total_supply' => (float)$item->total_supply,
                    'percent_change_1h' => (float)$item->percent_change_1h,
                    'percent_change_7d' => (float)$item->percent_change_7d,
                    'percent_change_24h' => (float)$item->percent_change_24h,
                    'market_cap_usd' => (float)$item->market_cap_usd,
                    'last_updated' => (int)$item->last_updated,
                ] //update values
            );

            $lastRecordDate = CurrenciesHistory::getLastHistoryRecordDateByCurrencyId($currencyId);

            if ($lastRecordDate <= (int)$item->last_updated - self::HISTORY_GAP_IN_SECONDS) {
                CurrenciesHistory::create(
                    ['currencies_id' => $currencyId,
                        'rank' => $item->rank,
                        'price_usd' => $item->price_usd,
                        'price_btc' => $item->price_btc,
                        '24h_volume_usd' => $item->volume,
                        'market_cap_usd' => $item->market_cap_usd,
                        'last_updated' => $item->last_updated
                    ]
                );

                $currenciesHistory = CurrenciesHistory::getLastRecordsForDayByCurrencyId($item->last_updated, $currencyId);

                CurrenciesChangeRates::updateOrCreate(
                    [
                        'currencies_id' => $currencyId
                    ],
                    [
                        'rank' => $this->calcMeanPropertyValue('rank', $currenciesHistory),
                        'price_usd' => $this->calcMeanPropertyValue('price_usd', $currenciesHistory),
                        'price_btc' => $this->calcMeanPropertyValue('price_btc', $currenciesHistory),
                        '24h_volume_usd' => $item->volume,
                        'market_cap_usd' => $this->calcMeanPropertyValue('market_cap_usd', $currenciesHistory),
                    ]
                );

            }
        }
    }
}

class Coins
{
    public $id_name, $name, $symbol, $rank, $price_usd, $price_btc, $volume,
        $market_cap_usd, $available_supply, $total_supply, $percent_change_1h, $percent_change_24h, $percent_change_7d, $last_updated;

    public function __construct($coin)
    {
        $this->id_name = $coin->id;
        $this->name = $coin->name;
        $this->symbol = $coin->symbol;
        $this->rank = $coin->rank;
        $this->price_usd = $coin->price_usd;
        $this->price_btc = $coin->price_btc;
        $this->volume = $coin->{'24h_volume_usd'};
        $this->market_cap_usd = $coin->market_cap_usd;
        $this->available_supply = $coin->available_supply;
        $this->total_supply = $coin->total_supply;
        $this->percent_change_1h = $coin->percent_change_1h;
        $this->percent_change_24h = $coin->percent_change_24h;
        $this->percent_change_7d = $coin->percent_change_7d;
        $this->last_updated = $coin->last_updated;


    }

}
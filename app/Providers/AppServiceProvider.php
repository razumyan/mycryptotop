<?php

namespace App\Providers;

use App\Services\CoinmarketDataFetcher;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

function boot()
{
    Schema::defaultStringLength(191);
}

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton('CoinmarketDataFetcher', function() {
            return new CoinmarketDataFetcher();
        });
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currencies extends Model
{
  protected $fillable = ['id_name', 'name', 'symbol'];
    //
  public function currenciesValues() {
    return $this->hasMany('App\Models\CurrenciesValues');
  }


  public function currenciesHistory() {
    return $this->hasMany('App\Models\CurrenciesHistory');
  }
}

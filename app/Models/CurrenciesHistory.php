<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrenciesHistory extends Model
{
    protected $fillable = ['currencies_id', 'rank', 'price_usd', 'price_btc', '24h_volume_usd', 'market_cap_usd', 'last_updated']; //anti mass assignment exception
    protected $table = "currencies_history";

    public static function getLastHistoryRecordDateByCurrencyId($currencyId)
    {
        $dateData = CurrenciesHistory::select('last_updated')
            ->where(['currencies_id' => $currencyId])
            ->orderByDesc('last_updated')
            ->limit(1)
            ->first();
        if ($dateData) {
            return (int)$dateData['last_updated'];
        }
        return 0;
    }

    public static function getLastRecordsForDayByCurrencyId($datePointTimestamp, $currencyId)
    {
        return CurrenciesHistory::select()
            ->where(['currencies_id' => $currencyId])
            ->where('last_updated', '>', $datePointTimestamp - 24 * 60 * 60)
            ->orderByDesc('last_updated')
            ->get();
    }

    public static function getLastRecordsForWeekByCurrencyId($datePointTimestamp, $currencyId)
    {
        return CurrenciesHistory::select()
            ->where(['currencies_id' => $currencyId])
            ->where('last_updated', '>', $datePointTimestamp - 7 * 24 * 60 * 60)
            ->orderByDesc('last_updated')
            ->get();
    }

    //
    public function values()
    {
        return $this->belongsTo('App\Models\Currencies');
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrenciesChangeRates extends Model
{
    protected $fillable = ['currencies_id', 'rank', 'price_usd', 'price_btc', '24h_volume_usd', 'market_cap_usd']; //anti mass assignment exception
    protected $table = "currencies_change_rates";

    //
    public function currencies()
    {
        return $this->hasOne('App\Models\Currencies', 'id', 'currencies_id');
    }

    public function currenciesValues()
    {
        return $this->hasOne('App\Models\CurrenciesValues', 'currencies_id', 'currencies_id');
    }
}
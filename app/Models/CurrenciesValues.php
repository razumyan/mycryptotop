<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrenciesValues extends Model
{
  protected $fillable = ['currencies_id', 'rank', 'price_usd', 'price_btc', '24h_volume_usd',
    'market_cap_usd', 'available_supply', 'total_supply', 'percent_change_1h','percent_change_24h',
    'percent_change_7d', 'last_updated']; //anti mass assignment exception

  public function currencies()
  {
    return $this->belongsTo('App\Models\Currencies');
  }
}

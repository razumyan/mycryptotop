<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Currencies;
use App\Models\CurrenciesValues;
use App\Models\CurrenciesHistory;

class UsersCurrenciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = Currencies::where('id', $id)
            ->where('user_id', Auth::user()->id);

        return $this->currenciesToJson($currencies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->currenciesToJson($currencies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->currenciesToJson($currencies);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return $this->currenciesToJson($currencies);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return $this->currenciesToJson($currencies);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->currenciesToJson($currencies);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        return $this->currenciesToJson($currencies);
    }


    /**
     * Create the specified resource for angular.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function currenciesToJson($currencies) {
        $items = [];
        foreach ($currencies->items as $item) {
            $data = $item->toArray();
            $data['currencies_id'] = $item->currencies_id;
            $items[] = $data;
        }
        $currenciesData = $currencies->toArray();
        $currenciesData['items'] = $items;
        return new Response($currenciesData);
    }
}

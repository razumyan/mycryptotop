<?php

namespace App\Http\Controllers;

use App\Models\CurrenciesChangeRates;
use App\Models\CurrenciesHistory;
use Illuminate\Http\Response;

class CurrencyChangeRatesController extends Controller
{
    private function assembleRates($rates)
    {
        $result = [];
        foreach($rates as $rate) {
            $coinDateStat = new CoinDateStat();
            $coin = new CoinMetadata();

            $coin->id = $rate->currencies->id_name;
            $coin->name = $rate->currencies->name;
            $coin->symbol = $rate->currencies->symbol;
            $coinDateStat->coin = $coin;

            $dayStat = new PriceStat();
            $dayStat->marketCapUsd = $rate->currenciesValues->market_cap_usd;
            $dayStat->priceUsd = $rate->currenciesValues->price_usd;
            $dayStat->priceBtc = $rate->currenciesValues->price_btc;
            $dayStat->rank = $rate->currenciesValues->rank;
            $dayStat->volume = $rate->currenciesValues->{'24h_volume_usd'};
            $coinDateStat->dayStat = $dayStat;

            $changeStat = new PriceStat();
            $changeStat->marketCapUsd = $dayStat->marketCapUsd - $rate->market_cap_usd;
            $changeStat->priceUsd = $dayStat->priceUsd - $rate->price_usd;
            $changeStat->priceBtc = $dayStat->priceBtc - $rate->price_btc;
            $changeStat->rank = $dayStat->rank - $rate->rank;
            $changeStat->volume = $dayStat->volume - $rate->{'24h_volume_usd'};
            $coinDateStat->changeStat = $changeStat;

            $chartData = [];
            $historyItems = CurrenciesHistory::getLastRecordsForWeekByCurrencyId($rate->last_update, $rate->currencies_id);
            foreach($historyItems as $historyItem) {
                $chartData[] = $historyItem->price_usd;
            }
            $coinDateStat->chartData = $chartData;

            $coinDateStat->timestamp =  $rate->currenciesValues->last_updated;

            $result[] = $coinDateStat;
        }
        return $result;
    }

    //
    public function index()
    {
        return new Response($this->assembleRates(CurrenciesChangeRates::get()));
    }
}

class CoinMetadata
{
    public $id,
        $name,
        $symbol;
}

class PriceStat
{
    public $rank,
        $priceUsd,
        $priceBtc,
        $marketCapUsd,
        $volume;
}

class CoinDateStat
{
    public $coin,
        $dayStat,
        $changeStat,
        $chartData,
        $timestamp;
}

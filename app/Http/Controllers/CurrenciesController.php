<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Currencies;
use App\Models\CurrenciesValues;

class CurrenciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($count)
    {
        //
        $currencies = Currencies::orderBy('rank', 'asc')
            ->take($count)
            ->get();
        $currenciesValues = $currencies->currenciesValues()
                ->with('currencies')
                ->join('currencies_values', 'currencies.id', '=', 'currencies_values.currencies_id')
                ->get(['currencies_values.*']);

        return $this->currenciesToJson($currenciesValues);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Create the specified resource for angular.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function currenciesToJson($currencies) {
        $items = [];
        foreach ($currencies->items as $item) {
            $data = $item->toArray();
            $data['marketCapUsd'] = $item->{'24h_volume_usd'};
            $items[] = $data;
        }
        $currenciesData = $currencies->toArray();
        $currenciesData['items'] = $items;
        return new Response($currenciesData);
    }
}

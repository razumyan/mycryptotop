<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_name');
            $table->string('name');
            $table->string('symbol');
            $table->timestamps();
        });
        Schema::create('currencies_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rank');
            $table->float('price_usd',32,8)->nullable();
            $table->float('price_btc',32,8)->nullable();
            $table->double('24h_volume_usd',16,2)->nullable();
            $table->double('market_cap_usd',16,2)->nullable();
            $table->double('available_supply',16,2)->nullable();
            $table->float('total_supply',16,2)->nullable();
            $table->float('percent_change_1h',8,2)->nullable();
            $table->float('percent_change_24h',8,2)->nullable();
            $table->float('percent_change_7d',8,2)->nullable();
            $table->integer('last_updated');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
        Schema::dropIfExists('currencies_values');
    }
}

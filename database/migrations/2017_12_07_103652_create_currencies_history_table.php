<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('currencies_id');
            $table->integer('rank');
            $table->float('price_usd',32,8)->nullable();
            $table->float('price_btc',32,8)->nullable();
            $table->double('24h_volume_usd',16,2)->nullable();
            $table->double('market_cap_usd',16,2)->nullable();
            $table->integer('last_updated');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies_history');
    }
}